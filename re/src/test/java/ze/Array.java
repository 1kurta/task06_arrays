package ze;




    public class Array<T extends Number> {


        T[] firstArray;

        T[] secondArray;



        Array(T[] first, T[] second) {
            firstArray = first;
            secondArray = second;
        }



        public void getJointArray(T[] third) {


            int k = 0;

            for (int index = 0; index < firstArray.length; index++) {
                for (int jdex = 0; jdex < secondArray.length; jdex++) {
                    if (firstArray[index] == secondArray[jdex]) {
                        third[k] = firstArray[index];
                        k++;
                    }
                }
            }

        }



        public void getDifferentArray(T[] third) {
            for(int index = 0; index< firstArray.length; index++) {
                for (int jdex = 0; jdex < secondArray.length; jdex++)
                    if (firstArray[index] == secondArray[jdex]) {
                        firstArray[index] = null;
                        secondArray[jdex] = null;
                    }
            }


            int k = 0;

            for(int index = 0; index< firstArray.length; index++)
            {
                if(firstArray[index]!=null) {
                    third[k] = firstArray[index];
                    k++;
                }else continue;
            }
            for(int index = 0;index<secondArray.length; index++){
                if(secondArray[index]!=null) {
                    third[k] = secondArray[index];
                    k++;
                } else continue;
            }
        }


        public static<T extends Number &Comparable<T>> void check(T[] arr) {

            sort(arr);
            T element;

            int reduceTheSizeOfIteration = 0;
            int hasOnly2 = 1;
            for(int index = 0; index <arr.length - reduceTheSizeOfIteration;index++) {
                element = arr[index];
                for(int jdex = index+1; jdex<arr.length - reduceTheSizeOfIteration; jdex++)
                {
                    if(arr[index].compareTo(arr[jdex])==0) {
                        hasOnly2++;
                    }
                }

                if(hasOnly2>2) {

                    hasOnly2=1;
                    reduceTheSizeOfIteration+=  deleteElement(arr,arr[index]);

                }
            }

        }



        private static <T extends  Number & Comparable<T>>  int deleteElement(T[] arr, T element) {

            int howMuch = 0;
            for(int index = 0; index<arr.length; index++) {
                if(arr[index].compareTo(element)==0) {
                    arr[index]=null;
                    howMuch++;
                }
            }

            for(int index = 0; index< arr.length-howMuch; index++) {

                if(arr[index]==null) {
                    arr[index] = arr[index+howMuch];
                    arr[index+howMuch] = null;
                }
            }

            return howMuch;

        }


        public static<T extends Number & Comparable<T>> void sort(T[] arr) {

            for(int index = 0; index<arr.length; index++) {
                for(int jdex = index+1; jdex<arr.length; jdex++) {

                    if(arr[index].compareTo(arr[jdex])>0) {
                        T temp = arr[index];
                        arr[index] = arr[jdex];
                        arr[jdex] = temp;
                    } else continue;
                }
            }
        }



        public static <T extends Number &Comparable<T>> void  deleteButLeaveOnly1(T[] arr) {


            for(int index = 0 ; index< arr.length-1; index++) {


                if(arr[index] == arr[index+1]) {
                    int jdex = index+1;
                    while(arr[jdex]==arr[index]) {
                        arr[jdex] = null;
                        jdex++;
                    }
                }
            }

            for(int index = 0; index<arr.length; index++) {
                if(arr[index]==null) {

                    for(int jdex = index+1; jdex<arr.length;jdex++) {
                        if(arr[jdex]!=null) {
                            arr[index] = arr[jdex];
                            arr[jdex] = null;
                            break;
                        }else continue;

                    }
                }
            }

        }



    }

