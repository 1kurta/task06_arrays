package shop;

import java.util.HashMap;
import java.util.Scanner;

public class Customer {

    public static void main(String[] args) {

        HashMap<String, Integer> purchasedItemsMap = new HashMap<String, Integer>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("\nWelcome to Online Shopping");

        do {

            System.out.println("\nSelect items to purchase :\n");

            System.out.println("1.  Toy 6000");
            System.out.println("2.  Mouse 800");
            System.out.println("3. Doll 500");
            System.out.println("4. Gamephone 200");
            System.out.println("5. Puzzle 50");

            System.out.println("\n6. Place Order");

            int choice = scanner.nextInt();

            if (choice == 1) {
                purchasedItemsMap.put("Toy", 6000);
            } else if (choice == 2) {
                purchasedItemsMap.put(" Mouse", 800);
            } else if (choice == 3) {
                purchasedItemsMap.put("Doll", 500);
            } else if (choice == 4) {
                purchasedItemsMap.put("Gamephone", 200);
            } else if (choice == 5) {
                purchasedItemsMap.put("Puzzle", 50);
            } else if (choice == 6) {
                Order order = new Order();
                CalculateBillServer calculateBillServer = new CalculateBillServer();
                order.order(purchasedItemsMap);
                System.out.println("Total bill amount is : " + calculateBillServer.calculateBill(purchasedItemsMap));
                break;
            } else {
                System.out.println("Wrong Choice !!!");
            }

        } while (true);

        System.out.println("\nApplication Terminated.");
        scanner.close();

    }

}