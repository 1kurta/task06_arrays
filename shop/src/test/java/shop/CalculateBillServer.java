package shop;

import java.util.HashMap;

public class CalculateBillServer {

    public long calculateBill(HashMap<String, Integer> purchasedItemsMap) {

        long totalBill = 0;

        for (long itemPrice : purchasedItemsMap.values()) {
            totalBill += itemPrice;
        }

        return totalBill;

    }

}
